"""Model for storing or deleteing an object in an S3 instance.

The object stores a pair of elements:
- key: the object name.
- value: the object itself.
"""

import boto3


class ModelS3:
    """Model for generating an S3 instance with a bucket, and store a tuple (key, value) in it.

    Args:
        object (object): base class of the hierarchy.
    """

    def __init__(self, bucket_name, resource_region, key, value):
        """Initialize the object fields: bucket name, region, name and value.

        Args:
            self (ModelS3): reference ot the instance itself.
            bucket_name (str): the bucket name.
            resource_region (str): the bucket region.
            key (str): the key of the object tuple.
            value (object): the value of the object tuple.
        """
        self.bucket_name = bucket_name
        self.resource_region = resource_region
        self.key = key
        self.value = value

    def save(self):
        """Saves the object instance in S3.

        Args:
            self (ModelS3): reference ot the instance itself.
        """
        s3_client = boto3.client("s3", region_name=self.resource_region)
        s3_client.put_object(Bucket=self.bucket_name, Key=self.key, Body=self.value)

    def exists_in_s3(self):
        """Checks if an object already exists in S3.
        It mainly prevents errors before deleting

        Args:
            self (ModelS3): reference ot the instance itself

        Returns:
            (bool): True if it exists, False if it does not exist
        """
        s3_resource = boto3.resource("s3", region_name=self.resource_region)
        bucket = s3_resource.Bucket(self.bucket_name)
        objs = list(bucket.objects.filter(Prefix=self.key))
        result = False
        if len(objs) > 0:
            for element in objs:
                result = result or (element.key == self.key)
        return result

    def delete(self):
        """Removes the object instance in S3 via its key.

        Args:
            self (ModelS3): reference ot the instance itself
        """
        s3_client = boto3.client("s3", region_name=self.resource_region)
        if self.exists_in_s3():
            s3_client.delete_object(Bucket=self.bucket_name, Key=self.key)
