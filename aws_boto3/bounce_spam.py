"""Spam handler, referring to checks on the 'dmarc' values from the event received

In case the file is valid it does no action
"""

import json
import datetime
import logging
import boto3

EMAIL_DOMAIN = "audemo.com"

# pylint: disable=W0613
def lambda_handler(event, context):
    """Spam filter, in AWS Lambda function format,
    which bounces the email according to 'dmarc' values.

    Args:
        event (dict): AWS Lambda uses this parameter to pass in event data to the handler
        content (dict): AWS Lambda uses this parameter to provide runtime information to
            your handler (default is None).

    Returns:
        (list): a list with the bounce results
    """
    bouncing = []
    for record in event["Records"]:
        ses_notification = record["ses"]
        message_id = ses_notification["mail"]["messageId"]
        receipt = ses_notification["receipt"]
        aws_region = record["awsRegion"]
        bouncing.append(__bounce_email(message_id, receipt, aws_region))
    return bouncing


def __bounce_email(message_id, receipt, aws_region):
    """Bounce the email according to 'dmarc' values.

    Args:
        message_id (str): the message identifier
        receipt (dict): the SES receipt value
        aws_region (str): the AWS location

    Returns:
        (dict): the bounced status
    """
    # Check DMARC
    if (
            receipt["dmarcVerdict"]["status"] == "FAIL"
            or receipt["dmarcPolicy"]["status"] == "FAIL"
    ):
        send_bounce_params = {
            "OriginalMessageId": message_id,
            "BounceSender": f"mailer-daemon@{EMAIL_DOMAIN}",
            "MessageDsn": {
                "ReportingMta": f"dns; {EMAIL_DOMAIN}",
                "ArrivalDate": datetime.datetime.utcnow().isoformat(),
            },
            "BouncedRecipientInfoList": [],
        }

        for recipient in receipt["recipients"]:
            send_bounce_params["BouncedRecipientInfoList"].append(
                {"Recipient": recipient, "BounceType": "ContentRejected"}
            )

        logging.debug(
            "Bouncing message with parameters: {}", json.dumps(send_bounce_params)
        )

        try:
            ses_client = boto3.client("ses", aws_region)
            bounce_response = ses_client.send_bounce(**send_bounce_params)
            logging.info(
                "Bounce for message {} sent, bounce message ID: {}",
                message_id,
                bounce_response["MessageId"]
            )
            return {"disposition": "stop_rule_set"}
        except Exception as ex:
            logging.error(ex)
            logging.error(
                "An error occurred while sending bounce for message: {}", message_id
            )
            raise ex
    else:
        logging.info("Accepting message: {}", message_id)

    return {}
