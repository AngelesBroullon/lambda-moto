""""
Handle an S3 with encryption
- get_file: retrieves a file and decrypts its content locally
- put a file: encrypts a file and puts it in the bucket

Based on https://github.com/tedder/s3-client-side-encryption/

Attributes:
    DEFAULT_CHUNKSIZE (int): the size of the chunk to cut the email when encrypting.
"""
import base64
import json
import struct
import os
import boto3
from Cryptodome.Cipher import AES
from Cryptodome import Random

DEFAULT_CHUNKSIZE = 16 * 1024


def get_file(bucket_name, bucket_region, key_name, dest_file):
    """
    Retrieves a file and decrypts its content locally.

    Args:
        bucket_name (srt): the bucket name, to access it.
        param bucket_region (str): the bucket region.
        key_name (str): the objetc key name, in order to filnd it.
        dest_file (str): the destination file to donwload it
    """
    dest_file_encrypted = dest_file + "-temp.enc"
    data_retrieved = __retrieve_from_s3(
        bucket_name, bucket_region, key_name, dest_file_encrypted
    )

    __decrypt_file(data_retrieved, dest_file_encrypted, dest_file)


def __retrieve_from_s3(bucket_name, bucket_region, key_name, dest_file):
    """
    Retrieves a file, which will be encrypted.

    Args:
        bucket_name (str): the bucket name, to access it.
        bucket_region (str): the bucket region.
        key_name (str): the objetc key name, in order to filnd it.
        dest_file (str): the destination file to donwload the encrypted version.

    Returns:
        (dict): a dictionary with "decrypted_envelope_key", "envelope_iv", "original_size",
        which will be needed for the decryption process.
    """
    s3_client = boto3.client("s3", bucket_region)
    object_info = s3_client.head_object(Bucket=bucket_name, Key=key_name)

    metadata = object_info["Metadata"]
    envelope_key = base64.b64decode(metadata["x-amz-key-v2"])
    envelope_iv = base64.b64decode(metadata["x-amz-iv"])
    encrypt_ctx = json.loads(metadata["x-amz-matdesc"])
    original_size = metadata["x-amz-unencrypted-content-length"]

    kms = boto3.client("kms", bucket_region)
    decrypted_envelope_key = kms.decrypt(
        CiphertextBlob=envelope_key, EncryptionContext=encrypt_ctx
    )

    s3_client.download_file(bucket_name, key_name, dest_file)
    return {
        "decrypted_envelope_key": decrypted_envelope_key["Plaintext"],
        "envelope_iv": envelope_iv,
        "original_size": int(original_size),
    }


def __decrypt_file(
        data_retrieved, input_filename, out_filename, chunksize=DEFAULT_CHUNKSIZE
):
    """
    Retrieves a file and decrypts its content locally.

    Args:
        data_retrieved (dict): fields "decrypted_envelope_key", "envelope_iv" and "original_size".
        out_filename (str): the destination file for the decrypted data.
        chunksize (int): size of the chunks in which we will download the file.
    """
    key = data_retrieved["decrypted_envelope_key"]
    init_vector = data_retrieved["envelope_iv"]
    original_size = data_retrieved["original_size"]

    with open(input_filename, "rb") as infile:
        decryptor = AES.new(key, AES.MODE_GCM, init_vector)

        with open(out_filename, "wb") as outfile:
            while True:
                chunk = infile.read(chunksize)
                if len(chunk) == 0:
                    break
                outfile.write(decryptor.decrypt(chunk))
            outfile.truncate(original_size)


def put_file(kms_arn, infile, bucket_name, service_region, key_name):
    """
    Encrypts a file and puts it in the bucket.

    Args:
        kms_arn (str): the key arn.
        bucket_name (str): the bucket in which we want to store the data.
        service_region (str): the area in which we want to store the data.
        key_name (str): the key the object will have.
    """
    encrypt_ctx = {"kms_cmk_id": kms_arn}
    key_data = __generate_key_data(kms_arn, encrypt_ctx, service_region)
    new_iv = Random.new().read(AES.block_size)
    size_infile = os.stat(infile).st_size  # unencrypted length
    outfile = infile + ".enc"

    data_retrieved = {
        "key_data": key_data["CiphertextBlob"],
        "new_iv": new_iv,
        "size_infile": size_infile,
        "encrypt_ctx": encrypt_ctx
    }

    __encrypt_file(
        key_data["Plaintext"], infile, new_iv, outfile, chunksize=DEFAULT_CHUNKSIZE
    )
    __put_file(data_retrieved, outfile, bucket_name, service_region, key_name)


def __generate_key_data(kms_arn, encrypt_ctx, service_region):
    """"
    Generates the data key from the context.

    Args:
        kms_arn (str): the key arn.
        encrypt_ctx (dict): the context.
        service_region (str): the AWS service region.

    Returns:
        (dict): the key_data ("Plaintext" and "CiphertextBlob").
    """
    kms = boto3.client("kms", service_region)
    return kms.generate_data_key(
        KeyId=kms_arn, EncryptionContext=encrypt_ctx, KeySpec="AES_256"
    )


def __encrypt_file(
        key, in_filename, init_vector, out_filename, chunksize=DEFAULT_CHUNKSIZE
):
    """
    Encrypts the file (in_filename) into the output-file.

    Args:
        key (str): the key cypherblob.
        in_filename (str): the input file name.
        init_vector (str): the initialization vector for generating the key.
        out_filename (str): the output file name.
        chunksize (int): the size of the chunk in case the file is too big.
    """
    with open(in_filename, "rb") as infile:
        cipher = AES.new(key, AES.MODE_GCM, init_vector)

        with open(out_filename, "wb") as outfile:
            last_chunk_length = 0
            while True:
                chunk = infile.read(chunksize)
                last_chunk_length = len(chunk)
                if last_chunk_length == 0 or last_chunk_length < chunksize:
                    break
                outfile.write(cipher.encrypt(chunk))

            # write the final padding
            length_to_pad = 16 - (last_chunk_length % 16)
            chunk += struct.pack("B", length_to_pad) * length_to_pad
            outfile.write(cipher.encrypt(chunk))


def __put_file(data_retrieved, upload_filename, bucket_name, service_region, key_name):
    """
    Stores the encrypted file into the bucket.

    Args:
        cyphertext_blob (str): encrypted blob.
        new_init_vector (str): the initialization vector for keys.
        encrypt_ctx (dict): the encryption context, to help decrypt.
        upload_filename (str): the file to upload.
        unencrypted_file_size (int): the size the file should have once unencrypted.
        bucket_name (str): the bucket in which we want to store the data.
        service_region (str): the area in which we want to store the data.
        key_name (str): the key the object will have.
    """
    cyphertext_blob = data_retrieved["key_data"]
    new_init_vector = data_retrieved["new_iv"]
    unencrypted_file_size = data_retrieved["size_infile"]
    encrypt_ctx = data_retrieved["encrypt_ctx"]

    matdesc_string = json.dumps(encrypt_ctx)
    metadata = {
        "x-amz-key-v2": base64.b64encode(cyphertext_blob).decode("utf-8"),
        "x-amz-iv": base64.b64encode(new_init_vector).decode("utf-8"),
        "x-amz-cek-alg": "AES/GCM/NoPadding",
        "x-amz-wrap-alg": "kms",
        "x-amz-matdesc": matdesc_string,
        "x-amz-unencrypted-content-length": str(unencrypted_file_size)
    }

    s3_client = boto3.client("s3", service_region)
    s3_transfer = boto3.s3.transfer.S3Transfer(s3_client)
    s3_transfer.upload_file(
        upload_filename, bucket_name, key_name, extra_args={"Metadata": metadata}
    )
