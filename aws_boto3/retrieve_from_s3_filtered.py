"""Retrieves an object from the S3 bucket, filtering with a certain prefix

It is pending to check if it may retrieve the bucket name from the event

Attributes:
    PREFIX (str): The bucket prefix, simulating a mailbox.
"""
import boto3

PREFIX = "demo.com/Email/Inbound/"

# pylint: disable=W0613
def lambda_handler(event, context):
    """Retrieves an object from the S3 bucket, filtering with a certain prefix

    Args:
        event (dict): AWS Lambda uses this parameter to pass in event data to the handler
        content (dict): AWS Lambda uses this parameter to provide runtime information to
            your handler (default is None).

    Returns:
        (list): the list of keys from the objects found
    """
    result = []
    for record in event["Records"]:
        bucket_name = record["s3"]["bucket"]["name"]
        aws_region = record["awsRegion"]
        s3_client = boto3.resource("s3", aws_region)
        my_bucket = s3_client.Bucket(bucket_name)

        for object_summary in my_bucket.objects.filter(Prefix=PREFIX):
            result = result.append(object_summary.key)
    return result
