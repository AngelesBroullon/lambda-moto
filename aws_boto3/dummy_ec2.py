"""Model for generating EC2 instances.

It provides a method to add a number of EC2 instances using the same AMI image.
"""

import boto3


class ModelEC2:
    """Model for generating EC2 instances.
    """

    def __init__(self, resource_region):
        """Initialize the object fields: region.

        Args:
            self (ModelEC2): reference ot the instance itself.
            resource_region (str): the bucket region.
        """
        self.resource_region = resource_region

    def add_servers(self, ami_id, count):
        """Generates a certain number of EC2 instances.
        The number of instances must be at least 1.

        Args:
            self (ModelEC2): reference of the instance itself.
            ami_id (str): AWS AMI indentifier, to know which image we want to
                run on the EC2 instances.
            count (int): AWS Lambda uses this parameter to pass in event data to the handler.

        Returns:
            (list): a list of instanceIds, from the new instances.
        """
        client = boto3.client("ec2", region_name=self.resource_region)
        instance_ids = []
        if count > 0:
            response = client.run_instances(
                ImageId=ami_id, MinCount=count, MaxCount=count
            )
            for instance in response["Instances"]:
                instance_ids.append(instance["InstanceId"])
        return instance_ids

    def start_servers(self, instances_ids_list):
        """Starts a list of servers via ids.

        self (ModelEC2): reference ot the instance itself.
        instances_ids_list (list): a list instance ids.

        Returns:
            (list): a list of json with the structure {id, status}.
        """
        client = boto3.client("ec2", region_name=self.resource_region)
        response = client.start_instances(InstanceIds=instances_ids_list)
        statuses = []
        for instances in response["StartingInstances"]:
            data_json = {
                "id": instances["InstanceId"],
                "status": instances["CurrentState"]["Name"],
            }
            statuses.append(data_json)
        return statuses

    def stop_servers(self, instances_ids_list):
        """Stops a list of servers via ids.

        Args:
            self (ModelEC2): reference ot the instance itself.
            instances_ids_list (list): a list instance ids.

        Returns:
            (list): a list of json with the structure {id, status}
        """
        client = boto3.client("ec2", region_name=self.resource_region)
        response = client.stop_instances(InstanceIds=instances_ids_list)
        statuses = []
        for instances in response["StoppingInstances"]:
            data_json = {
                "id": instances["InstanceId"],
                "status": instances["CurrentState"]["Name"],
            }
            statuses.append(data_json)
        return statuses
