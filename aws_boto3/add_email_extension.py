"""
Adds extension to a RAW email file on an S3 bucket
"""
import boto3

# pylint: disable=W0613
def lambda_handler(event, context):
    """Adds extension to a RAW email file on an S3 bucket,
    only if the file has not extension.

    Args:
        event (dict): AWS Lambda uses this parameter to pass in event data to the handler.
        context (dict): AWS Lambda uses this parameter to provide runtime information to
            your handler (default is None).
    """

    for record in event["Records"]:
        source_key = record["s3"]["object"]["key"]
        source_bucket = record["s3"]["bucket"]["name"]
        if len(source_key) < 3 or not source_key[len(source_key) - 4] == ".":
            __rename_file(source_bucket, source_key, source_key + ".eml")


def __rename_file(bucket_name, old_file_name, new_file_name):
    """
    Renames a file on a bucket.

    Args:
        bucket_name (str): the bucket name
        old_file_name (str): the original file name
        new_file_name (str): the new name for the file
    """
    s3_client = boto3.resource("s3")
    copy_source = bucket_name + "/" + old_file_name
    s3_client.Object(bucket_name, new_file_name).copy_from(CopySource=copy_source)
    s3_client.Object(bucket_name, old_file_name).delete()
