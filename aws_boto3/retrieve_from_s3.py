"""Retrieves an object from the S3 bucket

It is pending to check if it may retrieve the bucket name from the event
"""

import logging
import urllib.parse
import boto3


# pylint: disable=W0613
def lambda_handler(event, context):
    """Retrieves an object from the S3 bucket.

    Args:
        event (dict): AWS Lambda uses this parameter to pass in event data to the handler
        content (dict): AWS Lambda uses this parameter to provide runtime information to
            your handler (default is None).
    """

    # Get the objects from the event and show its content type
    for record in event["Records"]:
        bucket = record["s3"]["bucket"]["name"]
        aws_region = record["awsRegion"]
        key = urllib.parse.unquote_plus(
            record["s3"]["object"]["key"], encoding="utf-8"
        )
        try:
            s3_client = boto3.client("s3", region_name=aws_region)
            response = s3_client.get_object(Bucket=bucket, Key=key)
            return response["Body"].read()
        except Exception as ex:
            logging.info(ex)
            logging.info("Error getting object {} from bucket {}.", key, bucket)
            logging.info(
                "Make sure they exist and your bucket is in the same region as this function."
            )
            raise ex
