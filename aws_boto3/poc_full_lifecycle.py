"""Does a process with several steps:

1- extracting the id of an email from the event.
2- retrieving the email from s3.
3- process it to store the message on a certain bucket, and the attachment on a different one.

Attributes:
    DESTINATION_MESSAGE_BUCKET (str): The destination bucket for the message.
    DESTINATION_KEY (str): The bucket key.
"""

import json
import email
import os
import logging
import boto3


DESTINATION_MESSAGE_BUCKET = "destination_message_bucket"
DESTINATION_KEY = "destination_key"

# Set here for a seperate bucket otherwise it is set to the events bucket
DESTINATION_ATTACHMENT_BUCKET = "destination_attachment_bucket"
OUTPUT_PREFIX = "aeudemo.com/Attachments/Inbound/"  # Should end with /

DIR_PATH = os.path.dirname(os.path.realpath(__file__))
ATTACHMENT_DIR = "tmp"
FULL_ATTACHMENT_DIR = os.path.join(DIR_PATH, ATTACHMENT_DIR)

# pylint: disable=W0613
def lambda_handler(event, context):
    """Does a process of:
    - extracting the id of an email from the event.
    - retrieving the email from s3.
    - process it to store the message on a certain bucket, and the attachment on a different one.

    Args:
        event (dict): AWS Lambda uses this parameter to pass in event data to the handler.
        content (dict): AWS Lambda uses this parameter to provide runtime information to
            your handler (default is None).
    """

    for record in event["Records"]:
        # extract the contents from the emails and put the resulting object back to s3
        # get the source key and bucket from the event that triggered this lambda_function function.
        source_key = record["s3"]["object"]["key"]
        source_bucket = record["s3"]["bucket"]["name"]
        aws_region = record["awsRegion"]

        # get email from source s3 bucket
        msg = __get_message_from_bucket(source_bucket, aws_region, source_key)

        # process the email
        __process_email(aws_region, source_key, msg)


def __get_message_from_bucket(source_bucket, aws_region, source_key):
    """Retrieves a message from the bucket.

    Args:
        source_bucket (str): the bucket name where we want to search.
        aws_region (str): the AWS region.
        source_key (str): the file (message) name.
    """
    s3_client = boto3.client("s3", aws_region)
    s3_email_object = s3_client.get_object(Bucket=source_bucket, Key=source_key)[
        "Body"
    ].read()
    return email.message_from_bytes(s3_email_object)


def __process_email(aws_region, source_key, msg):
    """Process the email to split them in different buckets

    Args:
        aws_region (str): the AWS region.
        source_key (str): the file (message) name.
        msg (str): the message to process.
    """
    s3_resource = boto3.resource("s3", aws_region)
    payload_length = len(msg.get_payload())
    if payload_length > 0:
        # process attachments (payload[1], payload[2]...])
        list_fname = []
        if payload_length > 1:
            for index in range(1, payload_length):
                attachment = msg.get_payload()[index]
                disposition = attachment["Content-Disposition"]
                if disposition is not None:
                    file_name = str.split(disposition, "=")[index].replace('"', "")
                    __put_attachment_in_bucket(s3_resource, file_name, attachment)
                    list_fname.append(file_name)

        # process the message (payload[0])
        # get the email name from the key
        email_name = source_key.split("/")[-1]
        data_json = {
            "date": msg["Date"],
            "subject": msg["Subject"],
            "to": msg["To"],
            "from": msg["From"],
            "attachment": list_fname,
            "id": email_name,
        }
        logging.debug("json data {}", data_json)
        __put_message_in_bucket(s3_resource, email_name, data_json)


def __put_message_in_bucket(s3_resource, email_name, email_content_json):
    """Stores a message in the bucket destination bucket

    Args:
        s3_resource (boto3.session.Session.Resource): the S3 resource connection
        email_name (str): the bucket name where we want to store
        email_content_json (json): the message content in json format
    """
    full_s3_key = DESTINATION_KEY + "/" + email_name + ".json"
    s3_object = s3_resource.Object(DESTINATION_MESSAGE_BUCKET, full_s3_key)
    s3_object.put(Body=(bytes(json.dumps(email_content_json).encode("UTF-8"))))


def __put_attachment_in_bucket(s3_resource, file_name, attachment):
    """Stores a message in the bucket destination bucket.

    Args:
        s3_resource (boto3.session.Session.Resource): the S3 resource connection.
        file_name (str): the bucket name where we want to store
        attachment (object): the attachment object to store
    """
    __create_temporal_file(FULL_ATTACHMENT_DIR, file_name)
    with open(FULL_ATTACHMENT_DIR + "\\" + file_name, "wb") as file_object:
        file_object.write(attachment.get_payload(decode=True))
    full_file_name = FULL_ATTACHMENT_DIR + "\\" + file_name
    output_path = OUTPUT_PREFIX + file_name
    s3_resource.meta.client.upload_file(
        full_file_name, DESTINATION_ATTACHMENT_BUCKET, output_path
    )
    __remove_temporal_dummy_file(FULL_ATTACHMENT_DIR, file_name)


def __create_temporal_file(dir_path, filename):
    """Creates a temporal file on the `tmp` folder.
    It also creates the `tmp` folder if it did not exist.

    Args:
        dir_path (str): the file directory.
        filename (str): the file name.
    """
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    file_path = dir_path + "\\" + filename
    if not os.path.exists(file_path):
        with open(file_path, "w+", encoding="utf8") as file:
            file.close()


def __remove_temporal_dummy_file(dir_path, filename):
    """Removes a temporal file on the `tmp` folder,
    only if the folder exists.

    Args:
        dir_path (str): the file directory
        filename (str): the file name
    """
    if os.path.exists(dir_path):
        file_path = dir_path + "\\" + filename
        if os.path.exists(file_path):
            os.remove(file_path)
        os.rmdir(dir_path)
