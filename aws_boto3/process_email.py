"""Processes an email retrieved from an s3 bucket (SOURCE_BUCKET),
leaving the result in a different bucket (DESTINATION_BUCKET).

Attributes:
    DESTINATION_BUCKET (str): The name of the destination bucket.
    DESTINATION_KEY (str): the key of the bucket.
"""

import json
from email import policy
from email.parser import BytesParser
import logging
import boto3

DESTINATION_BUCKET = "destination_bucket"
DESTINATION_KEY = "s3/key/"


# pylint: disable=W0613
def lambda_handler(event, context):
    """Processes an email retrieved from an s3 bucket (SOURCE_BUCKET),
    leaving the result in a different bucket (DESTINATION_BUCKET)

    Args:
        event (dict): AWS Lambda uses this parameter to pass in event data to the handler
        content (dict): AWS Lambda uses this parameter to provide runtime information to
            your handler (default is None).

    Returns:
        (list): key from the email.
    """

    keys = []
    # get the records
    for record in event["Records"]:
        # extract the contents from the emails and put the resulting object back to s3
        # get the source key and bucket from the event that triggered this lambda_function function.
        source_key = record["s3"]["object"]["key"]
        source_bucket = record["s3"]["bucket"]["name"]
        aws_region = record["awsRegion"]

        # get the email name from the key
        email_name = source_key.split("/")[-1]

        # get email from s3
        s3_client = boto3.client("s3", aws_region)
        s3_object = s3_client.get_object(Bucket=source_bucket, Key=source_key)[
            "Body"
        ].read()
        email_object = __extract_contents(s3_object, email_name)

        # get the destination key and bucket from the environment variables
        full_s3_key = DESTINATION_KEY + email_name + ".json"

        # dump the result back to s3
        s3_client.put_object(
            Body=email_object, Bucket=DESTINATION_BUCKET, Key=full_s3_key
        )
        keys.append(full_s3_key)
    return keys


def __get_address_from_email(email_object, field):
    """Retrieves the email address from a json field.

    Args:
        email_object (json): the email json object.
        field (str): the field whose content we want to retrieve.

    Returns:
        (str): the email address.
    """
    if email_object[field]:
        return email_object[field].addresses[0].addr_spec
    return ""


def __get_date_field_from_email(email_object):
    """Retrieves a date field value from a json field.

    Args:
        email_object (object): the email json object.

    Returns:
        (str): the email date and time.
    """
    if email_object["Date"]:
        return str(email_object["Date"].datetime)
    return ""


def __extract_contents(message_object, email_name):
    """Extracts and formats some of the email content (id, from, to, cc, subject, date and body).

    Args:
        email_object (dict): the full email json object.
        email_name (str): the email name.

    Returns:
        (str): the content of the email as a json object, formated.
    """
    message_byte_object = BytesParser(policy=policy.default).parsebytes(message_object)

    email_object = {
        "id": email_name,
        "from": __get_address_from_email(message_byte_object, "From"),
        "to": __get_address_from_email(message_byte_object, "To"),
        "cc": __get_address_from_email(message_byte_object, "CC"),
        "subject": str(message_byte_object["subject"]),
        "date": __get_date_field_from_email(message_byte_object),
        "body": message_byte_object.get_body(preferencelist="plain").get_content(),
    }

    logging.info("email object {}", email_object)

    return json.dumps(email_object)
