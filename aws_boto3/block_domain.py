"""
Blocklist lambda for emails received

Attributes:
    BLOCKING_LIST (str): The blocking list name.
"""
import logging
import os


BLOCKING_LIST = "blocking_list"

# pylint: disable=W0613
def lambda_handler(event, context):
    """Blocks certain domains

    Args:
        event (dict): AWS Lambda uses this parameter to pass in event data to the handler
        content (dict): AWS Lambda uses this parameter to provide runtime information to
            your handler (default is None).

    Returns:
        (list): list with block result for the event records.
    """

    blocking = []
    for record in event["Records"]:
        ses_notification = record["ses"]
        mail_data = ses_notification["mail"]
        mail_source = mail_data["source"]
        mail_subject = mail_data["commonHeaders"]["subject"]
        message_id = mail_data["messageId"]
        receipt_recipients = ses_notification["receipt"]["recipients"]

        blocking_list_array = retrieve_block_list()

        if is_listed(blocking_list_array, mail_source):
            logging.info(
                "Rejecting messageId: {}  - Source: {} - Recipients: {} +  - Subject:  {}",
                message_id,
                mail_source,
                receipt_recipients,
                mail_subject,
            )
            blocking.append({"disposition": "STOP_RULE_SET"})
        else:
            logging.info(
                "Accepting messageId: {}  - Source: {} - Recipients: {} +  - Subject:  {}",
                message_id,
                mail_source,
                receipt_recipients,
                mail_subject,
            )
            blocking.append({})
    return blocking


def retrieve_block_list():
    """
    Convert the environment variable into array.
    Trim spaces from it.

    Returns:
        (list): a list fo string with the domains and addresses to block.
    """
    try:
        blocking_list_string = os.environ[BLOCKING_LIST]
        blocking_list_string = blocking_list_string.replace(" ", "")
        return blocking_list_string.split(",")
    except KeyError:
        logging.error("There is no blocklist environment variable")
        return ""


def is_listed(blocking_list_array, mail_source):
    """
    Check if the mail source matches with any of the email addresses,
    or domains defined in the environment variable.

    Args:
        blocking_list_array (list): a list blocked addresses and domains.
        mail_source(str): the email source.

    Returns:
        (bool): TRUE if if the mail source is in the list, otherwise FALSE.
    """
    for element in blocking_list_array:
        if mail_source.endswith(element):
            return True
    return False
