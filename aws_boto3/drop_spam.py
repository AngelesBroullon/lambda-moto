"""Spam filter, in AWS Lambda function format, which drops the email considered as spam.

In case the file is valid it does no action.
"""

import logging


# pylint: disable=W0613
def lambda_handler(event, context):
    """Spam filter, in AWS Lambda function format, which drops the email considered as spam.

    Args:
        event (dict): AWS Lambda uses this parameter to pass in event data to the handler.
        content (dict): AWS Lambda uses this parameter to provide runtime information to
            your handler (default is None).

    Returns:
        (list): a list with dropped status
    """
    dropping = []
    for record in event["Records"]:
        ses_notification = record["ses"]
        message_id = ses_notification["mail"]["messageId"]
        receipt = ses_notification["receipt"]
        dropping.append(__drop_spam(message_id, receipt))
    return dropping


def __drop_spam(message_id, receipt):
    """Spam filter, in AWS Lambda function format, which drops the email considered as spam.

    Args:
        message_id (str): the message identifier.
        receipt (dict): the message receipt data.

    Returns:
        (dict): the dropped status.
    """
    # Check the spam rules
    logging.debug("processing message: {}", message_id)
    if (
            receipt["spfVerdict"]["status"] == "FAIL"
            or receipt["dkimVerdict"]["status"] == "FAIL"
            or receipt["spamVerdict"]["status"] == "FAIL"
            or receipt["virusVerdict"]["status"] == "FAIL"
    ):
        return {"disposition": "stop_rule_set"}
    return {}
