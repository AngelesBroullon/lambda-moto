"""
Tests for email processing on `poc_full_lifecycle.py`

- Tests the case with mail with no attachments
- Tests the case with mail with one attachment
"""

import unittest
import os
import json
import boto3
from moto import mock_s3
from aws_boto3 import poc_full_lifecycle


@mock_s3
class TestPocFullCycle(unittest.TestCase):
    """Tests for email processing on `poc_full_lifecycle.py`

    Args:
        unittest.TestCase (unittest.TestCase): a class whose instances are test cases.

    Attributes:
        LOCATION (str): the AWs region.
        SOURCE_BUCKET (str): the source bucket name.
        DESTINATION_MESSAGE_BUCKET (str):  the destination bucket for messages.
        DESTINATION_KEY (str): the destination bucket key.
        DESTINATION_ATTACHMENT_BUCKET (str):  the destination bucket for attachments.
        OUTPUT_PREFIX (str): prefix for output.
        EMAIL_NO_ATTACHMENTS_NAME (str): EML file for demo email with no attachments.
        EMAIL_ONE_ATTACHMENT_NAME (str): EML file for demo email with one attachment file.
        DUMMY_FILE_NAME (str): a dummy attachment file.
    """

    __RESOURCE_TYPE = "s3"
    LOCATION = "eu-west-1"
    SOURCE_BUCKET = "source_bucket"
    DESTINATION_MESSAGE_BUCKET = "destination_message_bucket"
    DESTINATION_KEY = "destination_key"
    DESTINATION_ATTACHMENT_BUCKET = "destination_attachment_bucket"
    OUTPUT_PREFIX = "aeudemo.com/Attachments/Inbound/"
    __ATTACHMENT_DIR = "tmp"
    # emails
    __DIR_PATH = os.path.dirname(os.path.realpath(__file__))
    __FULL_ATTACHMENT_DIR = os.path.join(__DIR_PATH, __ATTACHMENT_DIR).replace(
        "test_aws_ses", "aws_ses"
    )
    EMAIL_NO_ATTACHMENTS_NAME = "dummy_email.eml"
    EMAIL_ONE_ATTACHMENT_NAME = "one_attachment_email.eml"
    DUMMY_FILE_NAME = "dummy_doc.docx"

    # setup and tear down test environment
    def setUp(self):
        """
        Prepares the testing environment.

        Args:
            self (TestPocFullCycle): reference to the instance itself.
        """
        conn = boto3.resource(self.__RESOURCE_TYPE, region_name=self.LOCATION)
        conn.create_bucket(
            Bucket=self.SOURCE_BUCKET,
            CreateBucketConfiguration={"LocationConstraint": self.LOCATION},
        )
        conn.create_bucket(
            Bucket=self.DESTINATION_MESSAGE_BUCKET,
            CreateBucketConfiguration={"LocationConstraint": self.LOCATION},
        )
        conn.create_bucket(
            Bucket=self.DESTINATION_ATTACHMENT_BUCKET,
            CreateBucketConfiguration={"LocationConstraint": self.LOCATION},
        )

    def tearDown(self):
        """
        Cleans up the data after the tests.

        Args:
            self (TestPocFullCycle): reference to the instance itself.
        """
        self.remove_bucket(self.SOURCE_BUCKET)
        self.remove_bucket(self.DESTINATION_MESSAGE_BUCKET)
        self.remove_bucket(self.DESTINATION_ATTACHMENT_BUCKET)

    @staticmethod
    def remove_bucket(bucket_name):
        """
        Removes a bucket from S3.
        It will delete its content before that.

        Args:
            bucket_name (str): the bucket name, to delete.
        """
        s3_bucket = boto3.resource(TestPocFullCycle.__RESOURCE_TYPE).Bucket(bucket_name)
        s3_bucket.objects.all().delete()
        s3_bucket.delete()

    def put_email_to_s3(self, test_email_path, email_name, bucket_name):
        """
        Stores an email object into S3

        Args:
            self (TestPocFullCycle): reference to the instance itself..
            test_email_path (str): the local email path, to retrieve the content
            email_name (str): the email name, which will be used as key.
            bucket_name (str): the bucket name, to delete
        """
        with open(test_email_path, "rb") as email_object:
            boto3.client(self.__RESOURCE_TYPE).put_object(
                Body=email_object, Bucket=bucket_name, Key=email_name
            )

    @staticmethod
    def get_s3_event_test(bucket_name, key):
        """
        Generates a dummy event value of the records in order to start to process.

        Args:
            bucket_name (str): the bucket to search into.
            key (str): the object key.

        Returns:
            (dict): a simulation of the event records.
        """
        return {
            "Records": [
                {
                    "eventVersion": "2.1",
                    "eventSource": "aws:s3",
                    "awsRegion": "eu-west-1",
                    "eventTime": "2020-05-06T09:52:02.539Z",
                    "eventName": "ObjectCreated:Put",
                    "userIdentity": {"principalId": "AWS:AIDAJ6K7DK6VAQXGPRIYC"},
                    "requestParameters": {"sourceIPAddress": "10.89.14.196"},
                    "responseElements": {
                        "x-amz-request-id": "85B55EECB9785243",
                        "x-amz-id-2": "pnEQcUtwITrV9NQvwfU5UR23",
                    },
                    "s3": {
                        "s3SchemaVersion": "1.0",
                        "configurationId": "d74c66d6-8c8c-4a66-8a90-287720b57710",
                        "bucket": {
                            "name": bucket_name,
                            "ownerIdentity": {"principalId": "AIHFFSGHH7X0"},
                            "arn": "arn:aws:s3:::euigs-shared-poc-email-s3",
                        },
                        "object": {
                            "key": key,
                            "size": 4091,
                            "eTag": "633582de694ae328a0a297e5ee8defc8",
                            "versionId": "Wlnh7SIw5Hi90ib5xduWmDuSR_JCvBby",
                            "sequencer": "005EB288C37871B3F5",
                        },
                    },
                }
            ]
        }

    def calculate_key(self, eml_name):
        """
        Calculates an object key.

        Args:
            self (TestPocFullCycle): reference to the instance itself.
            eml_name (str): the email object name

        Returns:
            (str): the object key calculated.
        """
        return self.DESTINATION_KEY + "/" + eml_name + ".json"

    # tests
    def test_no_attachment(self):
        """
        Test to extract an email with no attachments

        Args:
            self (TestPocFullCycle): reference to the instance itself.
        """
        # arrange
        event = self.get_s3_event_test(
            self.SOURCE_BUCKET, self.EMAIL_NO_ATTACHMENTS_NAME
        )

        test_email_path = os.path.join(
            self.__DIR_PATH, "resources", self.EMAIL_NO_ATTACHMENTS_NAME
        )
        self.put_email_to_s3(
            test_email_path, self.EMAIL_NO_ATTACHMENTS_NAME, self.SOURCE_BUCKET
        )

        # act
        poc_full_lifecycle.lambda_handler(event, None)

        # assert: retrieve and check the results recorded on s3
        s3_client = boto3.client(self.__RESOURCE_TYPE)
        full_destination_key = self.calculate_key(self.EMAIL_NO_ATTACHMENTS_NAME)
        result = s3_client.get_object(
            Bucket=self.DESTINATION_MESSAGE_BUCKET, Key=full_destination_key
        )
        json_message = json.loads(result["Body"].read().decode("utf-8"))
        assert (
            json_message["id"] == self.EMAIL_NO_ATTACHMENTS_NAME
            and len(json_message["attachment"]) == 0
        )

    def test_one_attachment(self):
        """
        Test to extract an email with one attachment

        Args:
            self (TestPocFullCycle): reference to the instance itself.
        """
        # arrange
        event = self.get_s3_event_test(
            self.SOURCE_BUCKET, self.EMAIL_ONE_ATTACHMENT_NAME
        )

        test_email_path = os.path.join(
            self.__DIR_PATH, "resources", self.EMAIL_ONE_ATTACHMENT_NAME
        )
        self.put_email_to_s3(
            test_email_path, self.EMAIL_ONE_ATTACHMENT_NAME, self.SOURCE_BUCKET
        )

        # act
        poc_full_lifecycle.lambda_handler(event, None)

        # assert
        s3_client = boto3.client(self.__RESOURCE_TYPE)
        full_destination_key = self.calculate_key(self.EMAIL_ONE_ATTACHMENT_NAME)
        # check email bucket
        result = s3_client.get_object(
            Bucket=self.DESTINATION_MESSAGE_BUCKET, Key=full_destination_key
        )
        json_message = json.loads(result["Body"].read().decode("utf-8"))
        assert (
            json_message["id"] == self.EMAIL_ONE_ATTACHMENT_NAME
            and len(json_message["attachment"]) == 1
        )
        # check attachment bucket
        output_key = self.OUTPUT_PREFIX + self.DUMMY_FILE_NAME
        result = s3_client.get_object(
            Bucket=self.DESTINATION_ATTACHMENT_BUCKET, Key=output_key
        )
        attached_content = result["Body"].read()
        assert attached_content is not None

    def test_no_existing_dir(self):
        """
        Launches the test with one attachment,
        making sure the folder tmp alreasy exists.

        Args:
            self (TestPocFullCycle): reference to the instance itself.
        """
        self.__create_temporal_folder()
        self.test_one_attachment()

    @staticmethod
    def __create_temporal_folder():
        """Creates a temporal file on the `tmp` folder.
        Creates the `tmp` folder if it did not exist.
        """
        dir_path = os.getcwd() + "\\aws_boto3\\tmp"
        if not os.path.exists(dir_path):
            os.makedirs(dir_path)
