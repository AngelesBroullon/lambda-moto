"""
Tests for dropping the email based on the event records, on `drop_spam.py`

- Test case with a valid email
- Test case with invalid email for invalid `spf_value`
- Test case with invalid email for invalid `dkim_value`
- Test case with invalid email for invalid `spam_value`
- Test case with invalid email for invalid `virus_value`
- Test case with invalid email for valid `spf_value`, `dkim_value`, `spam_value`, `virus_value`
"""

import unittest
from aws_boto3 import drop_spam


class TestDropSpam(unittest.TestCase):
    """Tests for dropping the spam based on the event records, on `drop_spam.py`

    Args:
        unittest.TestCase (unittest.TestCase): a class whose instances are test cases.

    Attributes:
        EMAIL_NAME (str): The blocking list name.
        DROP_MESSAGE (str): The stop message rule set.
    """

    EMAIL_NAME = "test_email.eml"
    DROP_MESSAGE = "stop_rule_set"

    # setup and tear down test environment
    @staticmethod
    def get_ses_event_test_spam_mail(spf_value, dkim_value, spam_value, virus_value):
        """Generates a dummy event value of the event records in order to bounce.

        Args:
            spf_value (str): flag with 'PASS' or 'FAIL' value.
            dkim_value (str): flag with 'PASS' or 'FAIL' value.
            spam_value (str): flag with 'PASS' or 'FAIL' value.
            virus_value (str): flag with 'PASS' or 'FAIL' value.

        Returns:
            (dict): a simulation of the records
        """

        return {
            "Records": [
                {
                    "eventVersion": "2.0",
                    "eventSource": "aws:ses",
                    "awsRegion": "eu-west-1",
                    "eventTime": "2016-09-25T05:15:44.261Z",
                    "eventName": "ObjectCreated:Put",
                    "userIdentity": {"principalId": "AWS:AROAW5C"},
                    "requestParameters": {"sourceIPAddress": "222.24.107.21"},
                    "responseElements": {
                        "x-amz-request-id": "00093EEAA5C7G7F2",
                        "x-amz-id-2": "9tTklyI/OEj",
                    },
                    "ses": {
                        "mail": {"messageId": "123456"},
                        "receipt": {
                            "spfVerdict": {"status": spf_value},
                            "dkimVerdict": {"status": dkim_value},
                            "spamVerdict": {"status": spam_value},
                            "virusVerdict": {"status": virus_value},
                        },
                    },
                }
            ]
        }

    # tests
    def test_drop_spam_spf_mail(self):
        """
        Case for dropping a spf flagged email.

        Args:
            self (TestDropSpam): reference of the instance itself.
        """
        event = self.get_ses_event_test_spam_mail("FAIL", "PASS", "PASS", "PASS")
        response = drop_spam.lambda_handler(event, None)
        verification = True
        for result in response:
            verification = verification and result["disposition"] == self.DROP_MESSAGE
        assert verification

    def test_drop_spam_dkim_mail(self):
        """
        Case for dropping a dkim flagged email.

        Args:
            self (TestDropSpam): reference of the instance itself.
        """
        event = self.get_ses_event_test_spam_mail("PASS", "FAIL", "PASS", "PASS")
        response = drop_spam.lambda_handler(event, None)
        verification = True
        for result in response:
            verification = verification and result["disposition"] == self.DROP_MESSAGE
        assert verification

    def test_drop_spam_spam_mail(self):
        """
        Case for dropping an spam flagged email.

        Args:
            self (TestDropSpam): reference of the instance itself.
        """
        event = self.get_ses_event_test_spam_mail("PASS", "PASS", "FAIL", "PASS")
        response = drop_spam.lambda_handler(event, None)
        verification = True
        for result in response:
            verification = verification and result["disposition"] == self.DROP_MESSAGE
        assert verification

    def test_drop_spam_virus_mail(self):
        """
        Case for dropping a virus flagged email.

        Args:
            self (TestDropSpam): reference of the instance itself.
        """
        event = self.get_ses_event_test_spam_mail("PASS", "PASS", "PASS", "FAIL")
        response = drop_spam.lambda_handler(event, None)
        verification = True
        for result in response:
            verification = verification and result["disposition"] == self.DROP_MESSAGE
        assert verification

    def test_drop_spam_valid_mail(self):
        """
        Case for detecting a valid email, which should not be dropped

        Args:
            self (TestDropSpam): reference of the instance itself.
        """
        event = self.get_ses_event_test_spam_mail("PASS", "PASS", "PASS", "PASS")
        response = drop_spam.lambda_handler(event, None)
        verification = True
        for result in response:
            verification = verification and result == {}
        assert verification
