"""
Test to retrieve data from S3, applying an email inbox filter

- Test case to try to retrieve content from s3 and finding no valid object
- Test case to try to retrieve content from s3 and finding a valid object
"""

import unittest
import boto3
from moto import mock_s3
from aws_boto3 import retrieve_from_s3_filtered


@mock_s3
class TestRetrieveFromS3Filtered(unittest.TestCase):
    """
    Test to retrieve data from S3, applying an email inbox filter.

    Args:
        unittest.TestCase (unittest.TestCase): a class whose instances are test cases.
    Attributes:
        SESSION_PROFILE_NAME (str): the session profile name
        RESOURCE_TYPE (str): the S3 bucket.
        BUCKET_NAME (str): = the S3 bucket name.
        LOCATION (str): = the AWs region.
        PREFIX (str): = the prefix to search.
        DATA (str): = the date as a string.
        EMAIL_NAME (str): = the eml file name.
    """

    SESSION_PROFILE_NAME = "demo-poc"
    RESOURCE_TYPE = "s3"
    BUCKET_NAME = "demo-poc-email-s3"
    LOCATION = "eu-west-1"
    PREFIX = "demo.com/Email/Inbound/"
    DATA = "0057E75D80IA35C3E0"
    EMAIL_NAME = "test_email.eml"

    # setup and tear down test environment
    def setUp(self):
        """
        Prepares the testing environment.

        Args:
            self (TestRetrieveFromS3Filtered): reference ot the instance itself.
        """
        conn = boto3.resource(self.RESOURCE_TYPE, region_name=self.LOCATION)
        conn.create_bucket(
            Bucket=self.BUCKET_NAME,
            CreateBucketConfiguration={"LocationConstraint": self.LOCATION},
        )

    def tearDown(self):
        """
        Cleans up the data after the tests.

        Args:
            self (TestRetrieveFromS3Filtered): reference ot the instance itself.
        """
        self.remove_bucket(self.BUCKET_NAME)

    @staticmethod
    def remove_bucket(bucket_name):
        """
        Cleans up after the tests.

        Args:
            self (TestRetrieveFromS3Filtered): reference ot the instance itself.
        """
        s3_bucket = boto3.resource(TestRetrieveFromS3Filtered.RESOURCE_TYPE).Bucket(
            bucket_name
        )
        s3_bucket.objects.all().delete()
        s3_bucket.delete()

    @staticmethod
    def get_s3_event(bucket_name, location, bucket_key):
        """
        Generates a dummy event value of the records in order to start to process.

        Args:
            bucket_name (str): the bucket to search into.
            location (str): the AWS region.
            bucket_key (str): the object key.

        Returns:
            (dict): a simulation of the event records.
        """
        return {
            "Records": [
                {
                    "eventVersion": "2.0",
                    "eventSource": "aws:s3",
                    "awsRegion": location,
                    "eventTime": "2016-09-25T05:15:44.261Z",
                    "eventName": "ObjectCreated:Put",
                    "userIdentity": {"principalId": "AWS:AROAW5C"},
                    "requestParameters": {"sourceIPAddress": "222.24.107.23"},
                    "responseElements": {
                        "x-amz-request-id": "00093EEAA5C7G7F2",
                        "x-amz-id-2": "9tTklyI/OEj",
                    },
                    "s3": {
                        "s3SchemaVersion": "1.0",
                        "configurationId": "151dfa64",
                        "bucket": {
                            "name": bucket_name,
                            "ownerIdentity": {"principalId": "A3QLJ3P3P5QY05"},
                            "arn": "arn:aws:s3:::" + bucket_name,
                        },
                        "object": {
                            "key": bucket_key,
                            "size": 11,
                            "eTag": "5eb63bbb",
                            "sequencer": "0057E75D80IA35C3E0",
                        },
                    },
                }
            ]
        }

    def put_on_s3(self, key, value):
        """
        Stores an object into S3.

        Args:
            self (TestRetrieveFromS3Filtered): reference ot the instance itself.
            key: the object key, to retrieve the content.
            value: the obejct value.
        """
        s3_client = boto3.client(self.RESOURCE_TYPE, region_name=self.LOCATION)
        s3_client.put_object(Bucket=self.BUCKET_NAME, Key=key, Body=value)

    # tests
    def test_retrieve_the_contents_of_s3_return_empty(self):
        """
        Test to retrieves values, getting none valid.

        Args:
            self (TestRetrieveFromS3Filtered): reference ot the instance itself.
        """
        # arrange
        event = self.get_s3_event(self.BUCKET_NAME, self.LOCATION, self.EMAIL_NAME)
        self.put_on_s3(self.EMAIL_NAME, self.DATA)

        # act
        retrieved_content = retrieve_from_s3_filtered.lambda_handler(event, None)

        # assert
        assert retrieved_content == []

    def test_retrieve_the_contents_of_s3_return_valid(self):
        """
        Test to retrieves values, getting one valid.

        Args:
            self (TestRetrieveFromS3Filtered): reference ot the instance itself.
        """
        # arrange
        email_name = self.PREFIX + self.EMAIL_NAME
        event = self.get_s3_event(self.BUCKET_NAME, self.LOCATION, email_name)
        self.put_on_s3(email_name, self.DATA)

        # act
        retrieved_content = retrieve_from_s3_filtered.lambda_handler(event, None)

        # assert
        assert retrieved_content != []
