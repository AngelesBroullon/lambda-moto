"""
Tests for bouncing the email based on the event records, on `bounce_spam.py`

- Test case with a valid email.
- Test case with invalid email for invalid `dmarc_verdict`.
- Test case with invalid email for invalid `dmarc_policy`.
"""

import unittest
import logging

# from SES.Client.exceptions import MessageRejected
from boto3.exceptions import Boto3Error
from aws_boto3 import bounce_spam
from unittest.mock import patch

class TestBounceSpam(unittest.TestCase):
    """Tests for bouncing the email based on the event records, on `bounce_spam.py`

    Args:
        unittest.TestCase (unittest.TestCase): a class whose instances are test cases.

    Attributes:
        EMAIL_NAME (str): the eml file name.
        DROP_MESSAGE (str): the "drop message" rule set.
        PROFILE_NAME (str): the profile name.
        RECIPIENT (str): the list of recipients as a json string.
    """

    EMAIL_NAME = "test_email.eml"
    DROP_MESSAGE = "stop_rule_set"
    PROFILE_NAME = "demo-poc"
    RECIPIENT = '{"Mr Potato"}'

    # setup and tear down test environment
    @staticmethod
    def get_ses_event_test_spam_mail(dmarc_verdict, dmarc_policy, recipients):
        """Generates a dummy event value of the event records in order to bounce.

        Args:
            dmar_verdict (str): flag with 'PASS' or 'FAIL' value.
            dmarc_policy (str): flag with 'PASS' or 'FAIL' value.
            recipients (str): the reciients data.

        Returns:
            (dict): a simulation of the records
        """
        return {
            "Records": [
                {
                    "eventVersion": "2.0",
                    "eventSource": "aws:ses",
                    "awsRegion": "eu-west-1",
                    "eventTime": "2016-09-25T05:15:44.261Z",
                    "eventName": "ObjectCreated:Put",
                    "userIdentity": {"principalId": "AWS:AROAW5C"},
                    "requestParameters": {"sourceIPAddress": "222.24.107.21"},
                    "responseElements": {
                        "x-amz-request-id": "00093EEAA5C7G7F2",
                        "x-amz-id-2": "9tTklyI/OEj",
                    },
                    "ses": {
                        "mail": {"messageId": "123456"},
                        "receipt": {
                            "dmarcVerdict": {"status": dmarc_verdict},
                            "dmarcPolicy": {"status": dmarc_policy},
                            "recipients": recipients,
                        },
                    },
                }
            ]
        }

    @staticmethod
    def get_mock_ses_client():
        """Generates a mock fo the SES client (SES.Client).

        The reason for having this individual mock is that moto does not have
        `send_bounce` implemented yet
        """
        base_client = unittest.mock.Mock()
        base_client.send_bounce.return_value = {"MessageId": "dummy_Id"}
        return base_client

    # tests
    def test_bounce_spam_valid_mail(self):
        """
        Case for detecting a valid email, which should not bounce.

        Args:
            self (TestBounceSpam): reference ot the instance itself.
        """
        event = self.get_ses_event_test_spam_mail("PASS", "PASS", "{}")
        result = bounce_spam.lambda_handler(event, None)
        assert result != self.DROP_MESSAGE

    def test_bounce_spam_dmarc_verdict_mail(self):
        """
        Case for detecting an invalid email for dmarc veredict reasons,
        which should bounce.

        The mock patch should be applied to the segment of code which needs it,
        or it may go beyond the test case (or event test class).

        Args:
            self (TestBounceSpam): reference ot the instance itself.
        """
        with unittest.mock.patch(
                "aws_boto3.bounce_spam.boto3.client",
                return_value=self.get_mock_ses_client(),
        ):
            event = self.get_ses_event_test_spam_mail("FAIL", "PASS", "{}")
            response = bounce_spam.lambda_handler(event, None)
            verification = True
            for result in response:
                verification = (
                    verification and result["disposition"] == self.DROP_MESSAGE
                )
            assert verification

    def test_bounce_spam_dmarc_policy_mail(self):
        """
        Case for detecting an invalid email for dmarc policy reasons,
        which should bounce.

        The mock patch should be applied to the segment of code which needs it,
        or it may go beyond the test case (or event test class).

        Args:
            self (TestBounceSpam): reference ot the instance itself.
        """
        with unittest.mock.patch(
                "aws_boto3.bounce_spam.boto3.client",
                return_value=self.get_mock_ses_client(),
        ):
            event = self.get_ses_event_test_spam_mail("PASS", "FAIL", "{}")
            response = bounce_spam.lambda_handler(event, None)
            verification = True
            for result in response:
                verification = (
                    verification and result["disposition"] == self.DROP_MESSAGE
                )
            assert verification

    def test_bounce_spam_dmarc_policy_mail_with_recipients(self):
        """
        Case for detecting an invalid email for dmarc policy reasons,
        which should bounce.

        The mock patch should be applied to the segment of code which needs it,
        or it may go beyond the test case (or event test class).

        Args:
            self (TestBounceSpam): reference ot the instance itself.
        """
        with unittest.mock.patch(
                "aws_boto3.bounce_spam.boto3.client",
                return_value=self.get_mock_ses_client(),
        ):
            event = self.get_ses_event_test_spam_mail("PASS", "FAIL", self.RECIPIENT)
            response = bounce_spam.lambda_handler(event, None)
            verification = True
            for result in response:
                verification = (
                    verification and result["disposition"] == self.DROP_MESSAGE
                )
            assert verification

    def test_bounce_spam_dmarc_policy_mail_with_recipients_bounce_failed(self):
        """
        Case for detecting an invalid email for dmarc policy reasons,
        which should bounce, but the bounce failed

        Args:
            self (TestBounceSpam): reference ot the instance itself.
        """
        mocked_client = self.get_mock_ses_client()
        mocked_client.send_bounce.side_effect = Boto3Error("Boom!")
        logging.debug("The client has been mocked with {}", mocked_client)
        with unittest.mock.patch(
                "aws_boto3.bounce_spam.boto3.client", return_value=mocked_client,
        ):
            error_handled = False
            event = self.get_ses_event_test_spam_mail("PASS", "FAIL", self.RECIPIENT)
            try:
                bounce_spam.lambda_handler(event, None)
            except Boto3Error as ex:
                logging.debug(ex)
                error_handled = True
            assert error_handled
