"""Tests for processing an email from s3, on `process_email`.

- Test case to extract content successfully
"""

import unittest
import os
import json
import boto3
from moto import mock_s3
from aws_boto3 import process_email


@mock_s3
class TestProcessEmail(unittest.TestCase):
    """Tests for processing an email from s3, on `process_email`.

    Args:
        unittest.TestCase (unittest.TestCase): a class whose instances are test cases.
    Attributes:
        LOCATION (str): the AWs region.
        SOURCE_BUCKET (str): the S3 source bucket name.
        DEST_BUCKET (str): the S3 destination bucket name.
        DEST_KEY (str): the destination bucket key.
        EMAIL_NAME (str): the eml file name.
        EMAIL_NAME_NO_DATE (str): the eml file name, which does not contain dates.
    """

    # environment
    __DIR_PATH = os.path.dirname(os.path.realpath(__file__))
    LOCATION = "eu-west-1"
    SOURCE_BUCKET = "source_bucket"
    DEST_BUCKET = "destination_bucket"
    DEST_KEY = "s3/key/"
    EMAIL_NAME = "dummy_email.eml"
    EMAIL_NAME_NO_DATE = "dummy_email_no_date.eml"
    __RESOURCE_S3 = "s3"

    def setUp(self):
        """
        Prepares the testing environment.

        Args:
            self (TestProcessEmail): reference ot the instance itself.
        """
        conn = boto3.resource(self.__RESOURCE_S3, region_name=self.LOCATION)
        conn.create_bucket(
            Bucket=self.SOURCE_BUCKET,
            CreateBucketConfiguration={"LocationConstraint": self.LOCATION},
        )
        conn.create_bucket(
            Bucket=self.DEST_BUCKET,
            CreateBucketConfiguration={"LocationConstraint": self.LOCATION},
        )

    def tearDown(self):
        """
        Cleans up after the tests.

        Args:
            self (TestProcessEmail): reference ot the instance itself.
        """
        self.remove_bucket(self.SOURCE_BUCKET)
        self.remove_bucket(self.DEST_BUCKET)

    @staticmethod
    def remove_bucket(bucket_name):
        """
        Removes a bucket from S3.
        It will delete its content before that.

        Args:
            self (TestProcessEmail): reference ot the instance itself.
        """
        s3_bucket = boto3.resource(TestProcessEmail.__RESOURCE_S3).Bucket(bucket_name)
        s3_bucket.objects.all().delete()
        s3_bucket.delete()

    @staticmethod
    def get_s3_event(bucket, key):
        """
        Generates a dummy event value of the records in order to start to process.

        Args:
            bucket_name (str): the bucket to search into.
            key (str): the object key.

        Returns:
            (dict): a simulation of the event records.
        """
        return {
            "Records": [
                {
                    "eventVersion": "2.0",
                    "eventSource": "aws:s3",
                    "awsRegion": "eu-west-1",
                    "eventTime": "2016-09-25T05:15:44.261Z",
                    "eventName": "ObjectCreated:Put",
                    "userIdentity": {"principalId": "AWS:AROAW5C"},
                    "requestParameters": {"sourceIPAddress": "222.24.107.21"},
                    "responseElements": {
                        "x-amz-request-id": "00093EEAA5C7G7F2",
                        "x-amz-id-2": "9tTklyI/OEj",
                    },
                    "s3": {
                        "s3SchemaVersion": "1.0",
                        "configurationId": "151dfa64",
                        "bucket": {
                            "name": bucket,
                            "ownerIdentity": {"principalId": "A3QLJ3P3P5QY05"},
                            "arn": "arn:aws:s3:::" + bucket,
                        },
                        "object": {
                            "key": key,
                            "size": 11,
                            "eTag": "5eb63bbc",
                            "sequencer": "0057E75D80IA35C3E0",
                        },
                    },
                }
            ]
        }

    @staticmethod
    def read_s3_object(bucket, key):
        """
        Retrieves an object from an S3 instance.

        Args:
            self (TestProcessEmail): reference to the instance itself,
            bucket_name (str): the bucket name,
            key (str): the object key,

        Returns:
            (object): the value of the object whose key we have provided.
        """
        object_read = boto3.client(TestProcessEmail.__RESOURCE_S3).get_object(
            Bucket=bucket, Key=key
        )
        return object_read["Body"].read()

    def put_email_to_s3(self, test_email_path, email_name):
        """
        Stores an email object into S3.

        Args:
            self (TestProcessEmail): reference to the instance itself.
            test_email_path (str): the local email path, to retrieve the content.
            email_name (str): the email name, which will be used as key.
            bucket_name (str): the bucket name, to delete.
        """
        with open(test_email_path, "rb") as email_object:
            boto3.client(self.__RESOURCE_S3).put_object(
                Body=email_object, Bucket=TestProcessEmail.SOURCE_BUCKET, Key=email_name
            )

    @staticmethod
    def get_expected_json(email_id, date):
        """
        Generates a dummy json to compare results

        Args:
            email_id (str): the email id
            date (str): the email date value

        Returns:
            (dict): the value of an expected json for the email
        """
        return {
            "id": email_id,
            "from": "dummy_email@mail.com",
            "to": "dummy_email_backup@mail.com",
            "cc": "",
            "subject": "This a test mail",
            "date": date,
            "body": "What's the buzz?",
        }

    def test_extract_the_contents_of_an_email_successfully(self):
        """
        Test to extract an email, with a valid result expected

        Args:
            self (TestProcessEmail): reference ot the instance itself.
        """
        # arrange
        test_email_path = os.path.join(self.__DIR_PATH, "resources", self.EMAIL_NAME)
        self.put_email_to_s3(test_email_path, self.EMAIL_NAME)
        event = self.get_s3_event(TestProcessEmail.SOURCE_BUCKET, self.EMAIL_NAME)

        # act
        s3_key_extracted_messages = process_email.lambda_handler(event, None)

        # asserts
        assert len(s3_key_extracted_messages) == 1

        email_as_json = self.read_s3_object(
            self.DEST_BUCKET, s3_key_extracted_messages[0]
        )
        self.assertDictEqual(
            json.loads(email_as_json),
            self.get_expected_json(self.EMAIL_NAME, "2020-05-04 13:42:54+02:00"),
        )

    def test_extract_the_contents_of_an_email_no_date(self):
        """
        Test to extract an email, with a valid result expected

        Args:
            self (TestProcessEmail): reference ot the instance itself.
        """
        # arrange
        test_email_path = os.path.join(
            self.__DIR_PATH, "resources", self.EMAIL_NAME_NO_DATE
        )
        self.put_email_to_s3(test_email_path, self.EMAIL_NAME_NO_DATE)
        event = self.get_s3_event(
            TestProcessEmail.SOURCE_BUCKET, self.EMAIL_NAME_NO_DATE
        )

        # act
        s3_key_extracted_messages = process_email.lambda_handler(event, None)

        # asserts
        assert len(s3_key_extracted_messages) == 1

        email_as_json = self.read_s3_object(
            self.DEST_BUCKET, s3_key_extracted_messages[0]
        )
        self.assertDictEqual(
            json.loads(email_as_json),
            self.get_expected_json(self.EMAIL_NAME_NO_DATE, ""),
        )
