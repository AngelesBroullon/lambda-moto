"""
Tests with string types, to learn the structure if unittest asserts.

- Test case for upper string.
- Test case for is_upper string.
- Test case for split string.
"""

import unittest


class TestStringMethods(unittest.TestCase):
    """
    Tests with string types, to learn the structure if unittest asserts.

    :param unittest.TestCase: a class whose instances are test cases.
    :type unittest.TestCase: unittest.TestCase.
    """

    def test_upper(self):
        """
        Test the upper method.

        Args:
            self (TestStringMethods): reference of the instance itself.
        """
        self.assertEqual("foo".upper(), "FOO")

    def test_isupper(self):
        """
        Test the is_upper method.

        Args:
            self (TestStringMethods): reference of the instance itself.
        """
        self.assertTrue("FOO".isupper())
        self.assertFalse("Foo".isupper())

    def test_split(self):
        """
        Test the split method.

        Args:
            self (TestStringMethods): reference of the instance itself.
        """
        dummy_string = "hello world"
        self.assertEqual(dummy_string.split(), ["hello", "world"])
        # check that s.split fails when the separator is not a string
        with self.assertRaises(TypeError):
            dummy_string.split(2)
