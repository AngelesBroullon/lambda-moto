"""
Tests for launching EC2 instances on `dummy_ec2.py`

- Test case to create one server
- Test case to create two servers
- Test case to create an invalid number of servers
"""
import unittest
import logging
import boto3
from moto import mock_ec2
from aws_boto3.dummy_ec2 import ModelEC2


@mock_ec2
class TestModelEC2(unittest.TestCase):
    """Tests for launching EC2 instances on `dummy_ec2.py`

    Args:
        unittest.TestCase (unittest.TestCase): a class whose instances are test cases.

    Attributes:
        EMAIL_NAME (str): The AWS region.
        DUMMY_AMI (str): an AMI identifier.
    """

    LOCATION = "eu-west-1"
    DUMMY_AMI = "ami-1234abcd"

    # test environment
    def tearDown(self):
        """
        Cleans up the data after the tests.

        Args:
            self (TestModelEC2): reference of the instance itself.
        """
        # terminate all ec2
        client = boto3.client("ec2", region_name=self.LOCATION)
        response = client.describe_instances()

        list_ids = []
        for reservation in response["Reservations"]:
            for instance in reservation["Instances"]:
                if instance["State"]["Name"] != "terminated":
                    list_ids.append(instance["InstanceId"])
        logging.debug("terminated: {}", list_ids)
        if list_ids != "[]":
            ec2 = boto3.resource("ec2", region_name=self.LOCATION)
            ec2.instances.filter(InstanceIds=list_ids).terminate()

    def test_add_servers_one_instance(self):
        """
        Test to launch a single EC2 instance.

        Args:
            self (TestModelEC2): reference of the instance itself.
        """
        # test
        model_ec2 = ModelEC2(self.LOCATION)
        instance_ids = model_ec2.add_servers(self.DUMMY_AMI, 1)

        # verify
        assert len(instance_ids) == 1

        client = boto3.client("ec2", region_name=self.LOCATION)
        reservations = client.describe_instances()["Reservations"]
        instances = reservations[len(reservations) - 1]["Instances"]
        assert len(instances) == 1
        instance = instances[0]
        assert instance["ImageId"] == self.DUMMY_AMI

    def test_add_servers_two_instances(self):
        """
        Test to launch 2 EC2 instances.

        Args:
            self (TestModelEC2): reference of the instance itself.
        """
        # test
        model_ec2 = ModelEC2(self.LOCATION)
        instance_ids = model_ec2.add_servers(self.DUMMY_AMI, 2)

        # verify
        assert len(instance_ids) == 2

        client = boto3.client("ec2", region_name=self.LOCATION)
        reservations = client.describe_instances()["Reservations"]
        instances = reservations[len(reservations) - 1]["Instances"]
        assert len(instances) == 2
        instance0 = instances[0]
        assert instance0["ImageId"] == self.DUMMY_AMI
        instance1 = instances[1]
        assert instance1["ImageId"] == self.DUMMY_AMI

    def test_add_servers_invalid_number_of_instance(self):
        """
        Test to launch a single EC2 instance.

        Args:
            self (TestModelEC2): reference of the instance itself.
        """
        # test
        model_ec2 = ModelEC2(self.LOCATION)
        instance_ids = model_ec2.add_servers(self.DUMMY_AMI, -1)

        # verify
        assert len(instance_ids) == 0
        client = boto3.client("ec2", region_name=self.LOCATION)
        reservations = client.describe_instances()["Reservations"]
        assert len(reservations) == 0

    def test_start_servers(self):
        """
        Test to launch a single EC2 instance.

        Args:
            self (TestModelEC2): reference of the instance itself.
        """
        # test
        model_ec2 = ModelEC2(self.LOCATION)
        instance_ids = model_ec2.add_servers(self.DUMMY_AMI, 1)
        response = model_ec2.start_servers(instance_ids)

        # verify
        start_failed_ids = []
        for element in response:
            if element["status"] != "pending":
                start_failed_ids.append(element["id"])
        assert len(start_failed_ids) == 0

    def test_stop_servers(self):
        """
        Test to launch a single EC2 instance.

        Args:
            self (TestModelEC2): reference of the instance itself.
        """
        # test
        model_ec2 = ModelEC2(self.LOCATION)
        instance_ids = model_ec2.add_servers(self.DUMMY_AMI, 1)
        response = model_ec2.stop_servers(instance_ids)

        # verify
        stop_failed_ids = []
        for element in response:
            if element["status"] != "stopping":
                stop_failed_ids.append(element["id"])
        assert len(stop_failed_ids) == 0
