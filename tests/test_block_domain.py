"""
Tests for blocking emails based on the event records, on `block_domain.py`

- Test case with a valid email
"""

import unittest
import os
import logging

from aws_boto3 import block_domain

class TestBlockEmail(unittest.TestCase):
    """Tests for blocking the email based on the event records, on `block_domain.py`

    Args:
        unittest.TestCase (unittest.TestCase): a class whose instances are test cases.
    """
    BLOCKING_LIST = "blocking_list"
    BLOCKING_DATA_VALUES = "mr.potato@mail.com, badmail.com"
    ACCEPTED_RECIPIENT = "demo_sender@mail.com"
    BLOCKED_RECIPIENT = "mr.potato@mail.com"
    BLOCKED_RECIPIENT_DOMAIN = "mr.potato@badmail.com"
    STOP_MESSAGE = "STOP_RULE_SET"

    # setup and tear down test environment
    def setUp(self):
        """
        Prepares the testing environment.

        Args:
            self (TestBlockEmail): reference ot the instance itself.
        """
        os.environ[self.BLOCKING_LIST] = self.BLOCKING_DATA_VALUES

    def tearDown(self):
        """
        Cleans up the data after the tests.

        Args:
            self (TestBlockEmail): reference ot the instance itself.
        """
        try:
            os.environ.pop(self.BLOCKING_LIST)
        except KeyError:
            logging.error("BLOCKING_LIST was already deleted")

    @staticmethod
    def get_ses_event_test_block_mail(mail_source):
        """Generates a dummy event value of the event records in order to bounce

        Args:
            mail_source (str): the email source, which may be blocked

        Returns:
            (dict): a simulation of the records
        """
        return {
            "Records": [
                {
                    "eventVersion": "1.0",
                    "eventSource": "aws:ses",
                    "awsRegion": "eu-west-1",
                    "ses": {
                        "mail": {
                            "timestamp": "2020-07-06T15:44:25.905Z",
                            "source": mail_source,
                            "messageId": "ht7uc2b5cftthb4ds3psmihfu89lptvg4dbsfu81",
                            "destination": [
                                "test@audemo.com"
                            ],
                            "commonHeaders": {
                                "returnPath": mail_source,
                                "from": [
                                    "Demo Sender <" + mail_source + ">"
                                ],
                                "date": "Mon, 6 Jul 2020 17:44:13 +0200",
                                "to": [
                                    "test@audemo.com"
                                ],
                                "messageId": "<123456@mail.gmail.com>",
                                "subject": "test block sender 1"
                            },
                        },
                        "receipt": {
                            "recipients": "[" + mail_source +  "]",
                        },
                    },
                }
            ]
        }

    # tests
    def test_block_email_no_recipients(self):
        """
        Case for detecting a valid email with no recipientsl, which should not bounce.

        Args:
            self (TestBlockEmail): reference of the instance itself.
        """
        event = self.get_ses_event_test_block_mail("{}")
        result = block_domain.lambda_handler(event, None)
        assert result != self.STOP_MESSAGE

    # tests
    def test_block_email_accepted_recipients(self):
        """
        Case for detecting a valid email with no recipientsl, which should not bounce.

        Args:
            self (TestBlockEmail): reference of the instance itself.
        """
        event = self.get_ses_event_test_block_mail(self.ACCEPTED_RECIPIENT)
        response = block_domain.lambda_handler(event, None)
        verification = True
        for result in response:
            verification = (
                verification and result == {}
            )
        assert verification

    def test_block_email_blocked_recipient(self):
        """
        Case for detecting a valid email with no recipientsl, which should not bounce.

        Args:
            self (TestBlockEmail): reference of the instance itself.
        """
        event = self.get_ses_event_test_block_mail(self.BLOCKED_RECIPIENT)
        response = block_domain.lambda_handler(event, None)
        verification = True
        for result in response:
            verification = (
                verification and result["disposition"] == self.STOP_MESSAGE
            )
        assert verification

    def test_block_email_blocked_recipient_domain(self):
        """
        Case for detecting a valid email with no recipientsl, which should not bounce.

        Args:
            self (TestBlockEmail): reference of the instance itself.
        """
        event = self.get_ses_event_test_block_mail(self.BLOCKED_RECIPIENT_DOMAIN)
        response = block_domain.lambda_handler(event, None)
        verification = True
        for result in response:
            verification = (
                verification and result["disposition"] == self.STOP_MESSAGE
            )
        assert verification

    def test_block_email_no_os_environ(self):
        """
        Case for detecting a valid email with no recipientsl, which should not bounce.

        Args:
            self (TestBlockEmail): reference of the instance itself.
        """
        os.environ.pop(self.BLOCKING_LIST)
        event = self.get_ses_event_test_block_mail("{}")
        result = block_domain.lambda_handler(event, None)
        assert result != self.STOP_MESSAGE
