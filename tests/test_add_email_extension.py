"""Tests for changing the extension to an email file on S3

- Test case to  change the name successfully
"""

import unittest
import logging
import os
import boto3
from botocore.exceptions import ClientError
from moto import mock_s3
from aws_boto3 import add_email_extension


@mock_s3
class TestAddEMailExtension(unittest.TestCase):
    """Tests for changing the extension to an email file on S3,
    referring to the code on `add_email_extension.py`.

    Args:
        unittest.TestCase (unittest.TestCase): a class whose instances are test cases.
    Attributes:
        SOURCE_BUCKET(str): the S3 bucket name.
        LOCATION (str): the AWs region.
        EMAIL_NAME_EML (str): EML email file name.
        EMAIL_NAME_RAW (str): RAW email file name.
        EMAIL_NAME_MINI_RAW (str): short RAW email file name
    """

    SOURCE_BUCKET = "source_bucket"
    LOCATION = "eu-west-1"
    EMAIL_NAME_EML = "dummy_email.eml"
    EMAIL_NAME_RAW = "dummy_raw_email"
    EMAIL_NAME_MINI_RAW = "dum"

    DIR_PATH = os.path.dirname(os.path.realpath(__file__))

    def setUp(self):
        """
        Prepares the testing environment.

        Args:
            self (TestAddEMailExtension): reference of the instance itself.
        """
        conn = boto3.resource("s3", region_name=self.LOCATION)
        conn.create_bucket(
            Bucket=self.SOURCE_BUCKET,
            CreateBucketConfiguration={"LocationConstraint": self.LOCATION},
        )

    def tearDown(self):
        """
        Cleans up after the tests.

        Args:
            self (TestAddEMailExtension): reference of the instance itself.
        """
        self.remove_bucket(self.SOURCE_BUCKET)

    @staticmethod
    def remove_bucket(bucket_name):
        """
        Removes a bucket from S3.
        It will delete its content before that.

        Args:
            bucket_name (str): the bucket name, to delete.
        """
        s3_bucket = boto3.resource("s3").Bucket(bucket_name)
        s3_bucket.objects.all().delete()
        s3_bucket.delete()

    @staticmethod
    def __read_s3_object(bucket, key):
        """
        Retrieves an object from an S3 instance.

        Args:
            self (TestAddEMailExtension): reference of the instance itself.
            bucket_name (str): the bucket name.
            key (str): the object key.

        Returns:
            (object): the value of the object whose key we have provided.
        Raises:
            (ClientError): in case there is no such key to find the object.
        """
        try:
            object_read = boto3.client("s3").get_object(Bucket=bucket, Key=key)
            return object_read["Body"].read()
        except ClientError as ex:
            if ex.response["Error"]["Code"] == "NoSuchKey":
                logging.info("No object found - returning empty")
                return None

    def __put_email_to_s3(self, test_email_path, email_name):
        """
        Stores an email object into S3.

        Args:
            self (TestAddEMailExtension): reference of the instance itself.
            test_email_path (str): the local email path, to retrieve the content.
            email_name (str): the email name, which will be used as key.
            bucket_name (str): the bucket name, to delete.
        """
        with open(test_email_path, "rb") as email_object:
            boto3.client("s3").put_object(
                Body=email_object, Bucket=self.SOURCE_BUCKET, Key=email_name
            )

    @staticmethod
    def get_s3_event(bucket, key):
        """
        Generates a dummy event value of the records in order to start to process.

        Args:
            bucket_name (str): the bucket to search into.
            key (str): the object key.

        Returns (dict): a simulation of the event records.
        """
        return {
            "Records": [
                {
                    "eventVersion": "2.0",
                    "eventSource": "aws:s3",
                    "awsRegion": "eu-west-1",
                    "eventTime": "2016-09-25T05:15:44.261Z",
                    "eventName": "ObjectCreated:Put",
                    "userIdentity": {"principalId": "AWS:AROAW5C"},
                    "requestParameters": {"sourceIPAddress": "222.24.107.21"},
                    "responseElements": {
                        "x-amz-request-id": "00093EEAA5C7G7F2",
                        "x-amz-id-2": "9tTklyI/OEj",
                    },
                    "s3": {
                        "s3SchemaVersion": "1.0",
                        "configurationId": "151dfa64",
                        "bucket": {
                            "name": bucket,
                            "ownerIdentity": {"principalId": "A3QLJ3P3P5QY05"},
                            "arn": "arn:aws:s3:::" + bucket,
                        },
                        "object": {
                            "key": key,
                            "size": 11,
                            "eTag": "5eb63bbc",
                            "sequencer": "0057E75D80IA35C3E0",
                        },
                    },
                }
            ]
        }

    def test_add_email_extension_raw_email(self):
        """
        Test to add the .eml extension to a raw email stored on S3.

        Args:
            self (TestAddEMailExtension): reference of the instance itself.
        """
        # arrange
        test_email_path = os.path.join(self.DIR_PATH, "resources", self.EMAIL_NAME_RAW)
        self.__put_email_to_s3(test_email_path, self.EMAIL_NAME_RAW)
        event = self.get_s3_event(self.SOURCE_BUCKET, self.EMAIL_NAME_RAW)

        # act
        add_email_extension.lambda_handler(event, None)

        # asserts
        object_old = self.__read_s3_object(self.SOURCE_BUCKET, self.EMAIL_NAME_RAW)
        object_new = self.__read_s3_object(
            self.SOURCE_BUCKET, self.EMAIL_NAME_RAW + ".eml"
        )
        assert object_new and (object_old is None)

    def test_add_email_extension_eml_email(self):
        """
        Test to add the .eml extension to a not raw email stored on S3.

        Args:
            self (TestAddEMailExtension): reference of the instance itself.
        """
        # arrange
        test_email_path = os.path.join(self.DIR_PATH, "resources", self.EMAIL_NAME_EML)
        self.__put_email_to_s3(test_email_path, self.EMAIL_NAME_EML)
        event = self.get_s3_event(self.SOURCE_BUCKET, self.EMAIL_NAME_EML)

        # act
        add_email_extension.lambda_handler(event, None)

        # asserts
        assert self.__read_s3_object(self.SOURCE_BUCKET, self.EMAIL_NAME_EML)

    def test_add_email_extension_mini_raw_email(self):
        """
        Test to add the .eml extension to a raw email stored on S3,
        which has a very short name.

        Args:
            self (TestAddEMailExtension): reference of the instance itself.
        """
        # arrange
        test_email_path = os.path.join(
            self.DIR_PATH, "resources", self.EMAIL_NAME_MINI_RAW
        )
        self.__put_email_to_s3(test_email_path, self.EMAIL_NAME_MINI_RAW)
        event = self.get_s3_event(self.SOURCE_BUCKET, self.EMAIL_NAME_MINI_RAW)

        # act
        add_email_extension.lambda_handler(event, None)

        # asserts
        object_old = self.__read_s3_object(self.SOURCE_BUCKET, self.EMAIL_NAME_MINI_RAW)
        object_new = self.__read_s3_object(
            self.SOURCE_BUCKET, self.EMAIL_NAME_MINI_RAW + ".eml"
        )
        assert object_new and (object_old is None)
