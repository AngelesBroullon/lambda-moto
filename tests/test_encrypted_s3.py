"""
Test cycle for encryption process
"""
import unittest
import filecmp
import os
import boto3
from moto import mock_s3
from moto import mock_kms
from aws_boto3 import encrypted_s3


@mock_s3
@mock_kms
class TestEncryptDecryptFile(unittest.TestCase):
    """
    Encryption cycle test.
    - Step 1: put and encrypt file
    - Step 2: get and decrypt file

    Args:
        unittest.TestCase (unittest.TestCase): a class whose instances are test cases.
    Attributes:
        SOURCE_BUCKET (str): the session profile name
        SERVICE_REGION (str): the S3 bucket.
        EMAIL_NAME (str): = the short email file name.
        EMAIL_XL_NAME (str): = the very big email file name.
        DUMMY_KEY (str): = key for storage.
    """

    SOURCE_BUCKET = "source_bucket"
    SERVICE_REGION = "eu-west-1"
    EMAIL_NAME = "dummy_email.eml"
    EMAIL_XL_NAME = "dummy_email_xl.eml"
    DUMMY_KEY = "Demo/First-Test-Email.eml"
    __DIR_PATH = os.path.dirname(os.path.realpath(__file__))

    # setup and tear down test environment
    def setUp(self):
        """
        Prepares the environment for the tests.

        Args:
            self (TestEncryptDecryptFile): reference ot the instance itself.
        """
        conn = boto3.resource("s3", self.SERVICE_REGION)
        conn.create_bucket(
            Bucket=self.SOURCE_BUCKET,
            CreateBucketConfiguration={"LocationConstraint": self.SERVICE_REGION},
        )

        kms = boto3.client("kms", self.SERVICE_REGION)
        response = kms.create_key(Description="KMS_KEY")
        self.__key_id = response["KeyMetadata"]["KeyId"]
        self.__arn = response["KeyMetadata"]["Arn"]

    def tearDown(self):
        """
        Cleans up the environment after the tests.

        Args:
            self (TestEncryptDecryptFile): reference ot the instance itself.
        """
        s3_bucket = boto3.resource("s3").Bucket(self.SOURCE_BUCKET)
        s3_bucket.objects.all().delete()
        s3_bucket.delete()

    def test_full_cycle_encrypt_decrypt(self):
        """
        Tests the process to encrypt a file, upload it, and then
        download it and decrypt it.

        Args:
            self (TestEncryptDecryptFile): reference ot the instance itself.
        """
        # arrange
        test_input_email_path = os.path.join(
            self.__DIR_PATH, "resources", self.EMAIL_NAME
        )
        test_output_email_path = os.path.join(
            self.__DIR_PATH, "resources", "output-" + self.EMAIL_NAME
        )

        # test encrypt and put
        encrypted_s3.put_file(
            self.__arn,
            test_input_email_path,
            self.SOURCE_BUCKET,
            self.SERVICE_REGION,
            self.DUMMY_KEY,
        )

        # test get and decrypt
        encrypted_s3.get_file(
            self.SOURCE_BUCKET,
            self.SERVICE_REGION,
            self.DUMMY_KEY,
            test_output_email_path,
        )

        # verify
        assert filecmp.cmp(test_input_email_path, test_output_email_path)

    def test_full_cycle_encrypt_decrypt_huge_file(self):
        """
        Tests the process to encrypt a file, upload it, and then
        download it and decrypt it.

        Args:
            self (TestEncryptDecryptFile): reference ot the instance itself.
        """
        # arrange
        test_input_email_path = os.path.join(
            self.__DIR_PATH, "resources", self.EMAIL_XL_NAME
        )
        test_output_email_path = os.path.join(
            self.__DIR_PATH, "resources", "output-" + self.EMAIL_XL_NAME
        )

        # test encrypt and put
        encrypted_s3.put_file(
            self.__arn,
            test_input_email_path,
            self.SOURCE_BUCKET,
            self.SERVICE_REGION,
            self.DUMMY_KEY,
        )

        # test get and decrypt
        encrypted_s3.get_file(
            self.SOURCE_BUCKET,
            self.SERVICE_REGION,
            self.DUMMY_KEY,
            test_output_email_path,
        )

        # verify
        assert filecmp.cmp(test_input_email_path, test_output_email_path)
