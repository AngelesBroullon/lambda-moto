"""
Tests for launching S3 instances on `dummy_s3.py`

- Tests the case with a valid data to save
- Tests the case with valid data overwritting previously existing data
"""

import unittest
import boto3
from moto import mock_s3
from aws_boto3.dummy_s3 import ModelS3


@mock_s3
class TestModelS3(unittest.TestCase):
    """Tests for launching S3 instances on `dummy_s3.py`.

    Args:
        unittest.TestCase (unittest.TestCase): a class whose instances are test cases.

    Attributes:
    RESOURCE_TYPE (str): = the S3 for buckets
    RESOURCE_NAME (str): = the bucket name.
    LOCATION (str): = the AWS region.

    DUMMY_KEY (str): = dummy key value
    DUMMY_VALUE (str): = dummy text value
    """

    # generate dummy data
    RESOURCE_TYPE = "s3"
    RESOURCE_NAME = "mybucket"
    LOCATION = "eu-west-1"

    DUMMY_KEY = "test object"
    DUMMY_VALUE = "everything is awesome"

    # test environment
    s3_client = None

    def setUp(self):
        """
        Prepares the testing environment.

        Args:
            self (TestModelS3): reference ot the instance itself.
        """
        # create bucket
        self.s3_client = boto3.resource(self.RESOURCE_TYPE, region_name=self.LOCATION)
        self.s3_client.create_bucket(
            Bucket=self.RESOURCE_NAME,
            CreateBucketConfiguration={"LocationConstraint": self.LOCATION},
        )

    def tearDown(self):
        """
        Cleans up the data after the tests.

        Args:
            self (TestModelS3): reference ot the instance itself.
        """
        # empty and delete the bucket
        bucket = self.s3_client.Bucket(self.RESOURCE_NAME)
        bucket.objects.all().delete()
        bucket.delete()

    def test_model_s3_save_invalid_bucket(self):
        """
        Test to save data unsuccessfully on S3, due to not finding the bucket

        Args:
            self (TestModelS3): reference ot the instance itself.
        """
        model_instance = ModelS3(
            "NonExistingBucket", self.LOCATION, self.DUMMY_KEY, self.DUMMY_VALUE
        )
        with self.assertRaises(Exception) as context:
            model_instance.save()
        exception_message = str(context.exception)
        message_expected = "NoSuchBucket"
        assert message_expected in exception_message

    def test_model_s3_save(self):
        """
        Test to save data successfully on S3

        Args:
            self (TestModelS3): reference ot the instance itself.
        """
        # test code
        model_instance = ModelS3(
            self.RESOURCE_NAME, self.LOCATION, self.DUMMY_KEY, self.DUMMY_VALUE
        )
        model_instance.save()

        # verify result
        body = (
            self.s3_client.Object(self.RESOURCE_NAME, self.DUMMY_KEY)
            .get()["Body"]
            .read()
            .decode("utf-8")
        )
        assert body == self.DUMMY_VALUE

    def test_model_s3_save_overwritting(self):
        """
        Test to save data successfully on S3, overwritting previous information

        Args:
            self (TestModelS3): reference ot the instance itself.
        """
        # test code
        model_instance = ModelS3(
            self.RESOURCE_NAME,
            self.LOCATION,
            self.DUMMY_KEY,
            self.DUMMY_VALUE + "demo",
        )
        model_instance.save()
        model_instance = ModelS3(
            self.RESOURCE_NAME, self.LOCATION, self.DUMMY_KEY, self.DUMMY_VALUE
        )
        model_instance.save()

        # verify result
        body = (
            self.s3_client.Object(self.RESOURCE_NAME, self.DUMMY_KEY)
            .get()["Body"]
            .read()
            .decode("utf-8")
        )
        assert body == self.DUMMY_VALUE

    def test_model_s3_exists_in_s3_false(self):
        """
        Test if the data key already exists on S3, in case it did not already

        Args:
            self (TestModelS3): reference ot the instance itself.
        """
        # test code
        model_instance = ModelS3(
            self.RESOURCE_NAME,
            self.LOCATION,
            self.DUMMY_KEY,
            self.DUMMY_VALUE + "demo",
        )

        # verify result
        assert not model_instance.exists_in_s3()

    def test_model_s3_exists_true(self):
        """
        Test if the data key already exists on S3, in case it already did

        Args:
            self (TestModelS3): reference ot the instance itself.
        """
        # test code
        model_instance = ModelS3(
            self.RESOURCE_NAME, self.LOCATION, self.DUMMY_KEY, self.DUMMY_VALUE
        )
        model_instance.save()

        # verify result
        assert model_instance.exists_in_s3()

    def test_model_s3_delete_not_existing(self):
        """
        Test to delete data which does not exist on S3

        Args:
            self (TestModelS3): reference ot the instance itself.
        """
        # test code
        model_instance = ModelS3(
            self.RESOURCE_NAME, self.LOCATION, self.DUMMY_KEY, self.DUMMY_VALUE + "demo"
        )
        model_instance.delete()

        # verify result
        assert not model_instance.exists_in_s3()

    def test_model_s3_delete_existing(self):
        """
        Test to delete data which does not exist on S3

        Args:
            self (TestModelS3): reference ot the instance itself.
        """
        # test code
        model_instance = ModelS3(
            self.RESOURCE_NAME, self.LOCATION, self.DUMMY_KEY, self.DUMMY_VALUE
        )
        model_instance.save()
        model_instance.delete()

        # verify result
        assert not model_instance.exists_in_s3()
