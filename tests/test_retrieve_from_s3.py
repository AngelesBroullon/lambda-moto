"""Test to retrieve data from S3, on `retrieve_from_s3.py`.

- Test case to retrieve content from s3 successfully
"""

import unittest
import boto3
from botocore.exceptions import ClientError
from moto import mock_s3
from aws_boto3 import retrieve_from_s3


@mock_s3
class TestRetrieveFromS3(unittest.TestCase):
    """
    Test to retrieve data from S3.

    Args:
        unittest.TestCase (unittest.TestCase): a class whose instances are test cases.

    Attributes:
        RESOURCE_TYPE (str): = S3 buckets,
        BUCKET_NAME (str):  = the name of the S3 bucket.
        LOCATION (str):  = the AWS region.
        EMAIL_NAME (str):  the .eml file name.
        DATA (str):  dummy data values
    """

    RESOURCE_TYPE = "s3"
    BUCKET_NAME = "dummy_bucket"
    LOCATION = "eu-west-1"
    EMAIL_NAME = "test_email.eml"
    DATA = "0057E75D80IA35C3E0"

    # setup and tear down test environment
    def setUp(self):
        """
        Prepares the testing environment.

        Args:
            self (TestRetrieveFromS3): reference to the instance itself.
        """
        conn = boto3.resource(self.RESOURCE_TYPE, region_name=self.LOCATION)
        conn.create_bucket(
            Bucket=self.BUCKET_NAME,
            CreateBucketConfiguration={"LocationConstraint": self.LOCATION},
        )

    def tearDown(self):
        """
        Cleans up the data after the tests.

        Args:
            self (TestRetrieveFromS3): reference to the instance itself.
        """
        s3_bucket = boto3.resource(self.RESOURCE_TYPE).Bucket(self.BUCKET_NAME)
        s3_bucket.objects.all().delete()
        s3_bucket.delete()

    @staticmethod
    def __get_s3_event(bucket_name, bucket_key):
        """
        Generates a dummy event value of the records in order to start to process.

        Args:
            param bucket_name (str): the bucket to search into.
            bucket_key (str): the object key.

        Returns:
            (dict): a simulation of the event records.
        """
        return {
            "Records": [
                {
                    "eventVersion": "2.0",
                    "eventSource": "aws:s3",
                    "awsRegion": "eu-west-1",
                    "eventTime": "2016-09-25T05:15:44.261Z",
                    "eventName": "ObjectCreated:Put",
                    "userIdentity": {"principalId": "AWS:AROAW5C"},
                    "requestParameters": {"sourceIPAddress": "222.24.107.23"},
                    "responseElements": {
                        "x-amz-request-id": "00093EEAA5C7G7F2",
                        "x-amz-id-2": "9tTklyI/OEj",
                    },
                    "s3": {
                        "s3SchemaVersion": "1.0",
                        "configurationId": "151dfa64",
                        "bucket": {
                            "name": bucket_name,
                            "ownerIdentity": {"principalId": "A3QLJ3P3P5QY05"},
                            "arn": "arn:aws:s3:::" + bucket_name,
                        },
                        "object": {
                            "key": bucket_key,
                            "size": 11,
                            "eTag": "5eb63bbb",
                            "sequencer": "0057E75D80IA35C3E0",
                        },
                    },
                }
            ]
        }

    # tests
    def test_retrieve_the_contents_of_s3(self):
        """
        Test to retrieve data successfully on S3

        Args:
            self (TestRetrieveFromS3): reference to the instance itself.
        """
        # arrange
        event = self.__get_s3_event(self.BUCKET_NAME, self.EMAIL_NAME)
        s3_client = boto3.client(self.RESOURCE_TYPE, region_name=self.LOCATION)
        s3_client.put_object(
            Bucket=self.BUCKET_NAME, Key=self.EMAIL_NAME, Body=self.DATA
        )

        # act
        retrieved_content = retrieve_from_s3.lambda_handler(event, None)

        # assert
        assert retrieved_content != self.DATA

    def test_retrieve_the_contents_of_s3_failed_object(self):
        """
        Test to retrieve data unsuccessfully on S3, no such object

        Args:
            self (TestRetrieveFromS3): reference to the instance itself.
        """
        # arrange
        event = self.__get_s3_event(self.BUCKET_NAME, self.EMAIL_NAME)

        # act
        retrieved_content = None
        try:
            retrieved_content = retrieve_from_s3.lambda_handler(event, None)
        except ClientError as ex:
            if ex.response["Error"]["Code"] == "NoSuchKey":
                retrieved_content = None

        # assert
        assert retrieved_content is None
