# Lambda Moto

## Definition

This project intends to generate a development environment to test AWS automation in Python3 in your local device, via the Software Develoment Kit [boto3](https://pypi.org/project/boto3/) for [Python 3](https://www.python.org/download/releases/3.0/).
The code provided defines several examples, and tests them using the [moto](https://pypi.org/project/moto/) mock with [unittest](https://docs.python.org/3/library/unittest.html).
The project setup also has a Gitlab CI/CD script in order to check quality of the code provided, via tests and code styles review.


## Content

### Code
* Set ups EC2 and S3 via python scripts.
	* `dummy_s3`: save and delete an object on an S3 bucket.
	* `encrypted_s3`: encrypts and saves on an S3, and it also downloads and decrypts from S3.
	* `dummy_ec2`: launches, starts and stops EC2 instances.
* Sets up some lambda functions for testing email processing.
	* `add_email_extension`: transforms a raw file (no extension) into an `.eml` file
	* `drop_spam`: detect if an email is spam through the metadata.
	* `block_domain`: blocks emails from certain senders.
	* `bounce_spam`: sets up the bounce functionality with the SES library.
	* `retrieve_from_s3`: gets an object from S3 stored on an S3 bucket.
	* `retrieve_from_s3_filtered`: filter case: retrieves data which came from a certain email folder.
	* `process_email`: moves email objects from a bucket to another one, handling the format issues.
* Simulates a pipeline process on `poc_full_lifecycle`, where:
* emails are retrieved from an s3 bucket.
* the messages are processed and go to a certain bucket.
* the attachments are processed and go to a different bucket.

### Tests
* The testing framework used is [unittest](https://pypi.org/project/unittest/).
* The [moto](https://pypi.org/project/moto/) package provides a faithfull mock for the [boto3](https://pypi.org/project/boto3/) library (AWS SDK for Python3).

### Code styles
* Code format is handled by [Black](https://pypi.org/project/black/).
* Code styles are checked with [Pylint](https://www.pylint.org/).



## Set up

* Upgrade pip (the Python default package manager).
	```bash
	python -m pip install --upgrade pip
	```

* Install AWS CLI (if you don't have it yet installed in your system).
	```bash
	python -m pip install awscli
	```

* Install boto3 (AWS API).
	```bash
	python -m pip install boto3
	```

* Install moto (AWS API mock).
	```bash
	python -m pip install moto
	```

* Install Cryptodomex (cryptography and encodings).
	```bash
	python -m pip install pycryptodomex
	```

* Install Black (formatter, optional).

	* Install package.
		```bash
		python -m pip install black
		```

	* How to configure it via VSCodium settings.
		* Go to 'Code -> Preferences -> Settings',
		* Search for "python formatting provider" and select "black" from the dropdown menu.
* Install Pylint (optional, in order to check code quality)
	* Generate `pylintrc` file.
		In case you have any issue while trying to run a manually generated `pylint`, it may be related to the file format, as it should be `utf-8` for in order to be properly parsed. Hence I recommend using the CLI command for this matter.
		```bash
		python -m pylint --generate-rcfile | out-file -encoding utf8 .pylintrc
		```
	* Run via CLI.
		```bash
		# run on code directory.
		python -m pylint "aws_boto3"
  		# run on tests directory.
		python -m pylint --load-plugins pylint_unittest "./tests/"
		```
* Install coverage
	```python
	python -m pip install coverage
	# run
	python -m coverage run -m --source="aws_boto3" unittest discover
	# get coverage report on CLI
	python -m coverage report
	# get html coverage
	python -m coverage html
	```

### Notes
* On files that simulate Lambda code, the following call is required: `def lambda_handler(event, context):`. However, the `context` variable is not usually used, so pylint rules mark it as an error as an "unused parameter". Hence, it is deactivated on the files via the `# pylint: disable=W0613` annotation.